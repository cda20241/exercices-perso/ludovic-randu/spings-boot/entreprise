
package com.entreprise.entreprise.repository;


import com.entreprise.entreprise.model.ServiceModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<ServiceModel,Long> {
    // la class ProjetRepository hérite de JpaRepository
    // JpaRepository fournit déjà toutes les fonctions les plus courantes de requêtes à la base de données
    // la fonction findById(), findAll(), create() etc...
    // la plupart du temps on n'a donc rien à faire de plus dans le repository
    // on doit faire 1 repository par class entity
}
