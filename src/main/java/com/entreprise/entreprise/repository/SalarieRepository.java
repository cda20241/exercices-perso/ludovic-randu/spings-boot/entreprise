
package com.entreprise.entreprise.repository;


import com.entreprise.entreprise.model.SalarieModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalarieRepository extends JpaRepository<SalarieModel,Long> {
    // la class JoueurRepository hérite de JpaRepository
    // JpaRepository fournit déjà toutes les fonctions les plus courantes de requêtes à la base de données
    // la fonction findById(), findAll(), create() etc...
    // la plupart du temps on n'a donc rien à faire de plus dans le repository
    // on doit faire 1 repository par class entity
}
