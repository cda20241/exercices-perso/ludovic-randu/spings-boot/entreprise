package com.entreprise.entreprise.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


// permet de définir que cette class est une class model (= entity) et donc que JPA doit s'occuper
// de mapper cette classe avec une table en base de données
@Getter
@Setter
@Entity
@Table(name = "service")
public class ServiceModel {
    @Id
    // permet de dire qu'on veut générer l'ID par auto-incrément
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // permet de dire que cet attribut de la classe sera enregistrée comme colonne dans la table
    // on peut avoir des attributs dans la class que l'on ne déclare pas en Column et donc ils ne seront
    // pas présents dans la table en base
    @Column(name="id",nullable = false,updatable = false)
    private Long id;

    @Column(length = 40)
    private String nom;


    @OneToMany
    private List<SalarieModel> salaries = new ArrayList<>();


    /*********************** CONSTRUCTEUR *********************/
    // je génère le ou les constructeurs dont j'ai besoin

    public ServiceModel() {
    }

    public ServiceModel(String nom) {
        this.nom = nom;
    }

    public ServiceModel(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    /*********************** GETTER et SETTER *********************/
    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }



    public void setId(Long id) {
        this.id = id;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }


}