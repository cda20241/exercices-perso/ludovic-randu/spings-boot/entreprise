package com.entreprise.entreprise.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


// permet de définir que cette class est une class model (= entity) et donc que JPA doit s'occuper
// de mapper cette classe avec une table en base de données
@Getter
@Setter
@Entity
@Table(name = "projet")
public class ProjetModel {
    @Id
    // permet de dire qu'on veut générer l'ID par auto-incrément
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // permet de dire que cet attribut de la classe sera enregistrée comme colonne dans la table
    // on peut avoir des attributs dans la class que l'on ne déclare pas en Column et donc ils ne seront
    // pas présents dans la table en base
    @Column(name="id",nullable = false,updatable = false)
    private Long id;

    @Column(length = 40)
    private String nom;

    @ManyToMany(mappedBy = "projets")
    @JsonBackReference
    private List<SalarieModel> salarie = new ArrayList<>();

    /*********************** CONSTRUCTEUR *********************/
    // je génère le ou les constructeurs dont j'ai besoin

    public ProjetModel() {
    }

    public ProjetModel(String nom) {
        this.nom = nom;
    }

    public ProjetModel(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    /*********************** GETTER et SETTER *********************/
    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
}