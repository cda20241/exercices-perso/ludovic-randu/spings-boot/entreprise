package com.entreprise.entreprise.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;


// permet de définir que cette class est une class model (= entity) et donc que JPA doit s'occuper
// de mapper cette classe avec une table en base de données
@Getter
@Setter
@Entity
@Table(name = "salarie")
public class SalarieModel {
    @Id
    // permet de dire qu'on veut générer l'ID par auto-incrément
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // permet de dire que cet attribut de la classe sera enregistrée comme colonne dans la table
    // on peut avoir des attributs dans la class que l'on ne déclare pas en Column et donc ils ne seront
    // pas présents dans la table en base
    @Column(name="id",nullable = false,updatable = false)
    private Long id;

    @Column(length = 40)
    private String nom;

    @Column(length = 40)
    private  String prenom;

    // Ajoutez ce champ pour la relation un-à-un avec AdresseModel
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_adresse")
    @JsonManagedReference
    private AdresseModel adresseModel;


    @ManyToOne
    @JoinColumn(name = "service_id", nullable = true)
    private  ServiceModel service;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "salarie_projet",
            joinColumns = @JoinColumn(name = "salarie_id"),
            inverseJoinColumns = @JoinColumn(name = "projet_id")
    )

    private Set<ProjetModel> projets = new HashSet<>();


    /*********************** CONSTRUCTEUR *********************/
    // je génère le ou les constructeurs dont j'ai besoin

    public SalarieModel() {
    }
    public SalarieModel(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public SalarieModel(String nom, String prenom, AdresseModel adresseModel, ServiceModel idService) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresseModel = adresseModel;
        this.service = idService;
    }

    public SalarieModel(Long id, String nom, String prenom, AdresseModel adresseModel, ServiceModel idService) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresseModel = adresseModel;
        this.service = idService;
    }

    /*********************** GETTER et SETTER *********************/
    public java.lang.Long getId() {
        return id;
    }

    public java.lang.String getNom() {
        return nom;
    }
    public java.lang.String getPrenom() {
        return prenom;
    }
    public AdresseModel getAdresseModel() {
        return adresseModel;
    }
    public ServiceModel getIdService() {
        return service;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }
    public void setNom(java.lang.String nom) {
        this.nom = nom;
    }
    public void setPrenom(java.lang.String prenom) {
        this.prenom = prenom;
    }
    public void setAdresseModel(AdresseModel adresseModel) {
        this.adresseModel = adresseModel;
    }
    public void setIdService(ServiceModel idService) {
        this.service = idService;
    }
}