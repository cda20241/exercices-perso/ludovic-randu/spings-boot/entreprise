package com.entreprise.entreprise.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


// permet de définir que cette class est une class model (= entity) et donc que JPA doit s'occuper
// de mapper cette classe avec une table en base de données
@Getter
@Setter
@Entity
@Table(name = "adresse")
public class AdresseModel {
    @Id
    // permet de dire qu'on veut générer l'ID par auto-incrément
    @GeneratedValue(strategy = GenerationType.AUTO)
    // permet de dire que cet attribut de la classe sera enregistrée comme colonne dans la table
    // on peut avoir des attributs dans la class que l'on ne déclare pas en Column et donc ils ne seront
    // pas présents dans la table en base
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column
    private String rue;

    @Column(length = 10)
    private Integer codePostal;

    @Column(length = 40)
    private String ville;

    @Column(length = 20)
    private String pays;

    // pour la relation un-à-un avec SalarieModel
    @OneToOne(mappedBy = "adresseModel", cascade = CascadeType.ALL)
    @JsonBackReference
    private SalarieModel salarieModel;

    /*********************** CONSTRUCTEUR *********************/
    // je génère le ou les constructeurs dont j'ai besoin

    public AdresseModel() {
    }

    public AdresseModel(String rue, Integer codePostal, String ville, String pays) {
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.pays = pays;
    }

    public AdresseModel(Long id, String rue, Integer codePostal, String ville, String pays) {
        this.id = id;
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.pays = pays;
    }

    /*********************** GETTER et SETTER *********************/
    public java.lang.Long getId() {
        return id;
    }
    public java.lang.String getRue() {
        return rue;
    }
    public java.lang.Integer getCodePostal() {
        return codePostal;
    }
    public java.lang.String getVille() {
        return ville;
    }
    public java.lang.String getPays() {
        return pays;
    }

    public SalarieModel getSalarieModel() {
        return salarieModel;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }
    public void setRue(java.lang.String rue) {
        this.rue = rue;
    }
    public void setCodePostal(java.lang.Integer codePostal) {
        this.codePostal = codePostal;
    }
    public void setVille(java.lang.String ville) {
        this.ville = ville;
    }
    public void setPays(java.lang.String pays) {
        this.pays = pays;
    }

    public void setSalarieModel(SalarieModel salarieModel) {
        this.salarieModel = salarieModel;
    }
}