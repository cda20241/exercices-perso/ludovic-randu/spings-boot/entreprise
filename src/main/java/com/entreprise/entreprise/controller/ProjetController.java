package com.entreprise.entreprise.controller;


import com.entreprise.entreprise.model.ProjetModel;
import com.entreprise.entreprise.repository.ProjetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/projet")
public class ProjetController {
    @Autowired
    private final ProjetRepository projetRepository;

    public ProjetController(ProjetRepository projetRepository) {
        // C'est Spring qui se charge d'instancier la classe ProjetController, et en le faisant il voit
        // qu'il doit aussi créer une instance deProjetRepository et nous la passer ici
        this.projetRepository = projetRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les joueurs de la base de donnée.
     *
     * @return une liste de Projet.
     */
    @GetMapping("/all")
    public List<ProjetModel> getAllProjet() {
        return projetRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un joueur selon son id.
     *
     * @param id
     * @return Une instance Joueur
     */
    @GetMapping("/{id}")
    public ProjetModel getProjet(@PathVariable Long id) {
        Optional<ProjetModel> projet = projetRepository.findById(id);
        if (projet.isPresent()) {
            return projet.get();
        } else {
            return null;
        }
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un joueur.
     *
     * @param projet
     * @return
     */
    @PostMapping("")
    public ResponseEntity<String> addProjet(@RequestBody ProjetModel projet) {
        // On set l'id a nul pour ne pas remplacer de projet, car on ne fait pas un update ici
        projet.setId(null);
        projet = this.projetRepository.save(projet);
        return new ResponseEntity<>("Ajout du projet '" + projet.getNom() + "'.", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un joueur.
     *
     * @param id
     * @param projet
     * @return une instance de Joueur modifiée
     */
    @PutMapping("/{id}")
    public ProjetModel updateProjet(@PathVariable Long id, @RequestBody ProjetModel projet) {
        projet.setId(id);
        return this.projetRepository.save(projet);
    }

    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un joueur.
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProjet(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le salarié avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<ProjetModel> projet = this.projetRepository.findById(id);
        if (projet.isPresent()) {
            this.projetRepository.deleteById(id);
            return new ResponseEntity<>("Suppression du projet " +id+ " terminé.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}