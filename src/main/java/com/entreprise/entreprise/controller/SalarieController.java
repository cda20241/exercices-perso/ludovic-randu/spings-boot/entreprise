package com.entreprise.entreprise.controller;


import com.entreprise.entreprise.model.AdresseModel;
import com.entreprise.entreprise.model.ProjetModel;
import com.entreprise.entreprise.model.SalarieModel;
import com.entreprise.entreprise.model.ServiceModel;
import com.entreprise.entreprise.repository.ProjetRepository;
import com.entreprise.entreprise.repository.SalarieRepository;
import com.entreprise.entreprise.repository.AdresseRepository;
import com.entreprise.entreprise.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@RestController
@RequestMapping("/salarie")
public class SalarieController {
    @Autowired
    private final SalarieRepository salarieRepository;
    private final AdresseRepository adresseRepository;
    private final ServiceRepository serviceRepository;
    private final ProjetRepository projetRepository;

    public SalarieController(SalarieRepository salarieRepository,
                             AdresseRepository adresseRepository,
                             ServiceRepository serviceRepository,
                             ProjetRepository projetRepository) {
        // C'est Spring qui se charge d'instancier la classe SalarieController, et en le faisant il voit
        // qu'il doit aussi créer une instance de SalarieRepository et nous la passer ici
        this.salarieRepository = salarieRepository;
        this.adresseRepository = adresseRepository;
        this.serviceRepository = serviceRepository;
        this.projetRepository = projetRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les joueurs de la base de donnée.
     *
     * @return une liste de Salarie.
     */
    @GetMapping("/all")
    public List<SalarieModel> getAllSalarie() {
        return salarieRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un joueur selon son id.
     *
     * @param id
     * @return Une instance Joueur
     */
    @GetMapping("/{id}")
    public SalarieModel getSalarie(@PathVariable Long id) {
        Optional<SalarieModel> salarie = salarieRepository.findById(id);
        if (salarie.isPresent()) {
            return salarie.get();
        } else {
            return null;
        }
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un joueur.
     *
     * @param salarie
     * @return
     */

    @PostMapping("")
    public ResponseEntity<String> addSalarie(@RequestBody SalarieModel salarie) {
        // On set l'id a nul pour ne pas remplacer de salarié, car on ne fait pas un update ici
        salarie.setId(null);
        // Gestion de l'adresse
        AdresseModel adresse = salarie.getAdresseModel();
        if (adresse != null) {
            adresse.setSalarieModel(salarie);
            adresseRepository.save(adresse); // Sauvegardez l'adresse en utilisant AdresseRepository
        }
        // Gestion du service
        ServiceModel service = salarie.getService();
        if (service != null) {
            service.getSalaries().add(salarie);
            serviceRepository.save(service); // Sauvegardez le service en utilisant ServiceRepository
        }

        salarie = this.salarieRepository.save(salarie);
        return new ResponseEntity<>("Ajout de " + salarie.getPrenom() + " " + salarie.getNom() + ".", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un joueur.
     *
     * @param id
     * @param newSalarie
     * @return une instance de Joueur modifiée
     */
    @PutMapping("/{id}")
    public SalarieModel updateSalarie(@PathVariable Long id, @RequestBody SalarieModel newSalarie) {
        return salarieRepository.findById(id).map(salarie -> {
            // Verification si le nom est non nul ou s'il est différent
            if (newSalarie.getNom() != null && !newSalarie.getNom().equals(salarie.getNom())) {
                salarie.setNom(newSalarie.getNom());
            }
            // Verification si le prenom est non nul ou s'il est différent
            if (newSalarie.getPrenom() != null && !newSalarie.getPrenom().equals(salarie.getPrenom())) {
                salarie.setPrenom(newSalarie.getPrenom());
            }
            if (newSalarie.getAdresseModel() != null && (salarie.getAdresseModel() == null ||
                  !newSalarie.getAdresseModel().getId().equals(salarie.getAdresseModel().getId()))) {
                        salarie.setAdresseModel(newSalarie.getAdresseModel());
            }
            if (newSalarie.getIdService() != null && (salarie.getIdService() == null ||
                  !newSalarie.getIdService().getId().equals(salarie.getIdService().getId()))) {
                        salarie.setIdService(newSalarie.getIdService());
            }
            // verification de la mise a jour projet
            if(newSalarie.getProjets() != null && !newSalarie.getProjets().equals(salarie.getProjets())) {
                if (newSalarie.getProjets() != null) {
                    // Créez un nouvel ensemble pour stocker les instances de ProjetModel gérées par JPA
                    Set<ProjetModel> managedProjets = new HashSet<>();
                    for (ProjetModel projet : newSalarie.getProjets()) {
                        // Récupérez chaque projet de la base de données
                        ProjetModel managedProjet = projetRepository.findById(projet.getId())
                                .orElseThrow();
                        managedProjets.add(managedProjet);
                    }
                    salarie.setProjets(managedProjets);
                }
            }

                // Faites de même pour tous les autres champs nécessaire
            return salarieRepository.save(salarie);
        })
        .orElseGet(() -> {
            newSalarie.setId(id);
            return salarieRepository.save(newSalarie);
        });
    }


    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un joueur.
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSalarie(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le salarié avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<SalarieModel> salarie = this.salarieRepository.findById(id);
        if (salarie.isPresent()) {
            this.salarieRepository.deleteById(id);
            return new ResponseEntity<>("Suppression du salarié "+id+ " terminée.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}