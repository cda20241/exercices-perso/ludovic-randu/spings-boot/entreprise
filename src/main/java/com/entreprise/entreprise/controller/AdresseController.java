package com.entreprise.entreprise.controller;

import com.entreprise.entreprise.model.AdresseModel;
import com.entreprise.entreprise.repository.AdresseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/adresse")
public class AdresseController {
    @Autowired
    private final AdresseRepository adresseRepository;

    public AdresseController(AdresseRepository adresseRepository) {
        // C'est Spring qui se charge d'instancier la classe SalarieController, et en le faisant il voit
        // qu'il doit aussi créer une instance de SalarieRepository et nous la passer ici
        this.adresseRepository = adresseRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les adresses de la base de donnée.
     *
     * @return une liste d'adresses'.
     */
    @GetMapping("/all")
    public List<AdresseModel> getAllAdresse() {
        return adresseRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un joueur selon son id.
     *
     * @param id
     * @return Une instance Joueur
     */
    @GetMapping("/{id}")
    public AdresseModel getAdresse(@PathVariable Long id) {
        Optional<AdresseModel> adresse = adresseRepository.findById(id);
        if (adresse.isPresent()) {
            return adresse.get();
        } else {
            return null;
        }
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un joueur.
     *
     * @param adresse
     * @return
     */
    @PostMapping("")
    public ResponseEntity<String> addAdresse(@RequestBody AdresseModel adresse) {
        // On set l'id a nul pour ne pas remplacer de'adresse, car on ne fait pas un update ici
        adresse.setId(null);
        adresse = this.adresseRepository.save(adresse);
        return new ResponseEntity<>("Ajout du " + adresse.getRue() + " " + adresse.getCodePostal() + ", "+
                    adresse.getVille()+", "+ adresse.getPays()+".", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un joueur.
     *
     * @param id
     * @param adresse
     * @return une instance de Joueur modifiée
     */
    @PutMapping("/{id}")
    public AdresseModel updateAdresse(@PathVariable Long id, @RequestBody AdresseModel adresse) {
        return this.adresseRepository.save(adresse);
    }

    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un joueur.
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJoueur(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le joueur avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<AdresseModel> adresse = this.adresseRepository.findById(id);
        if (adresse.isPresent()) {
            this.adresseRepository.deleteById(id);
            return new ResponseEntity<>("Suppression de l'adresse N°"+id+" terminée.",HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}