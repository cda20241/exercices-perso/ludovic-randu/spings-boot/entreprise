package com.entreprise.entreprise.controller;


import com.entreprise.entreprise.model.ServiceModel;
import com.entreprise.entreprise.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/service")
public class ServiceController {
    @Autowired
    private final ServiceRepository serviceRepository;

    public ServiceController(ServiceRepository serviceRepository) {
        // C'est Spring qui se charge d'instancier la classe ProjetController, et en le faisant il voit
        // qu'il doit aussi créer une instance deProjetRepository et nous la passer ici
        this.serviceRepository= serviceRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les joueurs de la base de donnée.
     *
     * @return une liste de Projet.
     */
    @GetMapping("/all")
    public List<ServiceModel> getAllService() {
        return serviceRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un joueur selon son id.
     *
     * @param id
     * @return Une instance Joueur
     */
    @GetMapping("/{id}")
    public ServiceModel getService(@PathVariable Long id) {
        Optional<ServiceModel> service = serviceRepository.findById(id);
        if (service.isPresent()) {
            return service.get();
        } else {
            return null;
        }
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un joueur.
     *
     * @param service
     * @return
     */
    @PostMapping("")
    public ResponseEntity<String> addService(@RequestBody ServiceModel service) {
        // On set l'id a nul pour ne pas remplacer de projet, car on ne fait pas un update ici
        service.setId(null);
        service = this.serviceRepository.save(service);
        return new ResponseEntity<>("Enregistrement de " + service.getNom() + " effectué.", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un joueur.
     *
     * @param id
     * @param service
     * @return une instance de Joueur modifiée
     */
    @PutMapping("/{id}")
    public ServiceModel updateService(@PathVariable Long id, @RequestBody ServiceModel service) {
        service.setId(id);
        return this.serviceRepository.save(service);
    }

    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un joueur.
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteService(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le salarié avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<ServiceModel> service = this.serviceRepository.findById(id);
        if (service.isPresent()) {
            this.serviceRepository.deleteById(id);
            return new ResponseEntity<>("Suppression du service "+id+ " terminé.",HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}